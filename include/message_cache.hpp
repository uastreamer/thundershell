#ifndef __MESSAGE_CACHE_HPP__
#define __MESSAGE_CACHE_HPP__

#include <deque>
#include <iostream>
#include <mutex>
#include "common.hpp"
#include "singleton.hpp"



class MessageCache: public Singleton<MessageCache>
{
public:
    ~MessageCache();

    message_ptr create();
    message_ptr create(message_type);
    void destroy(message*);
    void set_pool_size(const uint32_t);
private:
    uint32_t id_{0};
    std::deque<message*> pool_;
    uint32_t pool_size_{1024};
    std::mutex mtx_;
};

//----------------------------------------------------------------------------------------------------------------------

inline MessageCache::~MessageCache()
{
    for (auto ptr: pool_) {
        if (ptr) {
            delete ptr;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

inline message_ptr MessageCache::create()
{
    message* ptr{nullptr};
    message_ptr handled_ptr{nullptr};
    uint32_t message_id{0};

    {
        std::lock_guard<decltype(mtx_)> lck(mtx_);
        if (!pool_.empty()) {
            ptr = pool_.front();
            pool_.pop_front();
        } else {
            ptr = new message();
        }

        // incrementing message id
        message_id = id_;
        ++id_;
    }

    if (ptr) {
        auto deleter = [this](auto ptr) {
            this->destroy(ptr);
        };

        handled_ptr = std::unique_ptr<message, decltype(deleter)>(ptr, std::move(deleter));

        // resetting message
        handled_ptr->type = message_type::undefined;
        handled_ptr->id = message_id;
        handled_ptr->connection_id = 0;
        handled_ptr->data_len = 0;
    }

    if (!handled_ptr) {
        std::cout << __func__ << " UNABLE TO CREATE MESSAGE\n";
    }

    return handled_ptr;
}

//----------------------------------------------------------------------------------------------------------------------

inline message_ptr MessageCache::create(message_type type)
{
    auto handled_ptr = create();
    if (handled_ptr) {
        handled_ptr->type = type;
    }

    return handled_ptr;
}

//----------------------------------------------------------------------------------------------------------------------

inline void MessageCache::destroy(message* ptr)
{
    if (ptr) {
        std::lock_guard<decltype(mtx_)> lck(mtx_);

        if (pool_.size() < pool_size_) {
            pool_.emplace_back(std::move(ptr));
        } else {
            delete ptr;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

inline void MessageCache::set_pool_size(const uint32_t pool_size)
{
    pool_size_ = pool_size;
}


#endif // __MESSAGE_CACHE_HPP__

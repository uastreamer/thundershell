#ifndef __COMMON_HPP__
#define __COMMON_HPP__

#include <array>
#include <functional>
#include <limits>
#include <memory>
#include <random>



constexpr int64_t HEARTBEAT_TIMEOUT = 5 * 1000; // in milliseconds
constexpr int64_t HEARTBEAT_MAX_TIMEOUT = HEARTBEAT_TIMEOUT * 3; // in milliseconds
constexpr uint16_t MESSAGE_DATA_LEN = 1408; // header + data must not violate default MTU 1500

//----------------------------------------------------------------------------------------------------------------------

enum class message_type: uint16_t
{
    undefined = 0,
    hello,
    heartbeat_request,
    heartbeat_reply,
    connection_created,
    connection_ended_by_src,
    connection_ended_by_dst,
    data,
    undefined_max
};

//----------------------------------------------------------------------------------------------------------------------

struct message
{
    message_type type = message_type::undefined;
    uint16_t id = 0;
    uint16_t connection_id = 0;
    uint16_t data_len = 0;
    std::array<uint8_t, MESSAGE_DATA_LEN> data;
};

//----------------------------------------------------------------------------------------------------------------------

using message_ptr = std::unique_ptr<message, std::function<void(message*)>>;

constexpr uint16_t MESSAGE_HEADER_LEN = sizeof(message().type)
                                        + sizeof(message().id)
                                        + sizeof(message().connection_id)
                                        + sizeof(message().data_len);
constexpr uint16_t MESSAGE_MAX_LEN = MESSAGE_HEADER_LEN + MESSAGE_DATA_LEN;
constexpr uint16_t MESSAGE_MIN_LEN = MESSAGE_HEADER_LEN;

//----------------------------------------------------------------------------------------------------------------------

inline uint16_t random_id()
{
    int32_t from = std::numeric_limits<uint16_t>::min();
    int32_t to = std::numeric_limits<uint16_t>::max();
    std::uniform_int_distribution<int32_t> dist(from, to);

    std::random_device rd;
    std::mt19937 engine(rd());

    return dist(engine);
}


#endif // __COMMON_HPP__

#ifndef __MESSAGE_HPP__
#define __MESSAGE_HPP__

#include <cstring>
#include "common.hpp"
#include "message_cache.hpp"



template<typename T>
inline void message_build(const message_ptr& msg, T* t, const uint16_t bytes)
{
    std::memcpy(msg.get(), t, bytes);
}

//----------------------------------------------------------------------------------------------------------------------

inline message_ptr message_ptr_create()
{
    auto message_cache = MessageCache::instance();
    auto handled_ptr = message_cache->create();

    return handled_ptr;
}

//----------------------------------------------------------------------------------------------------------------------

inline message_ptr message_ptr_create(message_type type)
{
    auto message_cache = MessageCache::instance();
    auto handled_ptr = message_cache->create(type);

    return handled_ptr;
}

//----------------------------------------------------------------------------------------------------------------------

template<typename T>
inline void message_create(message* msg, T* t, const uint16_t bytes)
{
    std::memcpy(msg, t, bytes);
}

//----------------------------------------------------------------------------------------------------------------------

inline uint16_t message_len(const message& msg)
{
    uint16_t len = MESSAGE_HEADER_LEN + msg.data_len;

    return len;
}

//----------------------------------------------------------------------------------------------------------------------

inline uint16_t message_ptr_len(const message_ptr& msg)
{
    uint16_t len = MESSAGE_HEADER_LEN + msg->data_len;

    return len;
}

//----------------------------------------------------------------------------------------------------------------------

template<typename T>
void message_set_data(message* msg, const T& t, const uint16_t len)
{
    if (len <= MESSAGE_DATA_LEN) {
        std::copy(t.begin(), t.begin() + len, msg->data.begin());
        msg->data_len = len;
    } else {
        throw std::runtime_error("Unable to set message data, size is too long " + std::to_string(len)
                                 + " > max message len " + std::to_string(MESSAGE_DATA_LEN) + ".");
    }
}

//----------------------------------------------------------------------------------------------------------------------

inline void message_set_data(message* msg, const uint8_t* data, const uint16_t len)
{
    if (len <= MESSAGE_DATA_LEN) {
        std::memcpy(msg->data.data(), data, len);
        msg->data_len = len;
    } else {
        throw std::runtime_error("Unable to set message data, size is too long " + std::to_string(len)
                                 + " > max message len " + std::to_string(MESSAGE_DATA_LEN) + ".");
    }
}

//----------------------------------------------------------------------------------------------------------------------

template<typename T>
void message_ptr_set_data(const message_ptr& msg, const T& t, const uint16_t len)
{
    if (len <= MESSAGE_DATA_LEN) {
        std::copy(t.begin(), t.begin() + len, msg->data.begin());
        msg->data_len = len;
    } else {
        throw std::runtime_error("Unable to set message data, size is too long " + std::to_string(len)
                                 + " > max message len " + std::to_string(MESSAGE_DATA_LEN) + ".");
    }
}

//----------------------------------------------------------------------------------------------------------------------

inline void message_ptr_set_data(const message_ptr& msg, const uint8_t* data, const uint16_t len)
{
    if (len <= MESSAGE_DATA_LEN) {
        std::memcpy(msg->data.data(), data, len);
        msg->data_len = len;
    } else {
        throw std::runtime_error("Unable to set message data, size is too long " + std::to_string(len)
                                 + " > max message len " + std::to_string(MESSAGE_DATA_LEN) + ".");
    }
}

//----------------------------------------------------------------------------------------------------------------------

inline bool message_valid(const message& msg)
{
    bool valid{true};

    if (msg.data_len > MESSAGE_DATA_LEN) {
        valid = false;
    }
    if ((msg.type == message_type::undefined) || (msg.type >= message_type::undefined_max)) {
        valid = false;
    }

    return valid;
}

//----------------------------------------------------------------------------------------------------------------------

inline bool message_valid(const message& msg, const uint16_t bytes)
{
    bool valid{false};

    auto msg_len = message_len(msg);
    if ((msg_len >= MESSAGE_MIN_LEN) && (msg_len <= MESSAGE_MAX_LEN)) {
        if (bytes == (msg.data_len + MESSAGE_HEADER_LEN)) {
            return true;
        }
    }

    return valid;
}


#endif // __MESSAGE_HPP__

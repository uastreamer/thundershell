#ifndef __SINGLETON_HPP__
#define __SINGLETON_HPP__

#include <memory>
#include <utility>



template<typename T>
class Singleton
{
public:
    template<typename... Args>
    static T* instance(Args... args)
    {
        if (!instance_) {
            instance_ = std::make_shared<T>(std::forward<Args>(args)...);
        }

        return instance_.get();
    }
private:
    static std::shared_ptr<T> instance_;
};

//----------------------------------------------------------------------

template<typename T>
std::shared_ptr<T> Singleton<T>::instance_ = nullptr;


#endif // __SINGLETON_HPP__

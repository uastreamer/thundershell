#ifndef __REMOTE_SERVICE_HPP__
#define __REMOTE_SERVICE_HPP__

#include <sys/epoll.h>
#include <chrono>
#include <deque>
#include <map>
#include <string>
#include "common.hpp"
#include "local_connection.hpp"



class RemoteService
{
public:
    RemoteService(std::string&&, const uint16_t, std::string&&);

    void launch();
private:
    bool connection_established(const int32_t);
    bool io_scheduler_add_socket(const int32_t);
    bool io_scheduler_add_socket_out_event(const int32_t);
    bool io_scheduler_del_socket_out_event(const int32_t);
    bool io_scheduler_del_socket(const int32_t);
    int64_t get_timeout();
    void handle_local_connection_data(const uint16_t, const int64_t);
    void handle_local_connection_ended(const uint16_t);
    void handle_messages(std::deque<message_ptr>&&);
    void handle_message_connection_created(message_ptr&&);
    void handle_message_connection_ended_by_src(message_ptr&&);
    void handle_message_data(message_ptr&&);
    void handle_network_event(const int32_t, const int32_t);
    void handle_socket_write_ready(const int32_t);
    void handle_socket_read_ready(const int32_t);
    void handle_timeout_reached();
    void initialize_epoll();
    void make_local_connection(const uint16_t);
    void make_remote_connection();
    void on_remote_connect();
    void on_remote_disconnect();
    void process_remote_data(const int64_t);
    void read_remote_data();
    void remove_local_connection(const std::shared_ptr<LocalConnection>&);
    void send_heartbeat_message();
    void send_hello_message();
    void send_to_remote();
    void send_to_remote_buffered();
    void service_loop();
    void update_remote_last_activity();

    bool connected_{false};
    std::string remote_host_{""};
    uint16_t remote_port_{0};
    int32_t remote_socket_{0};
    std::string node_name_{""};
    int32_t ep_socket_{0};
    struct epoll_event ep_event_;
    static constexpr uint32_t ep_event_size_{128};
    std::map<uint16_t, std::shared_ptr<LocalConnection>> local_connections_by_connection_id_;
    std::map<int32_t, std::shared_ptr<LocalConnection>> local_connections_by_socket_;
    std::array<uint8_t, MESSAGE_MAX_LEN * 128> local_connections_receiving_buffer_;
    std::array<uint8_t, MESSAGE_MAX_LEN * 128> receiving_buffer_;
    uint64_t receiving_buffer_received_bytes_{0};
    uint64_t receiving_buffer_received_bytes_offset_{0};
    std::array<uint8_t, MESSAGE_MAX_LEN> sending_buffer_;
    uint16_t sending_buffer_unsent_bytes_{0};
    uint16_t sending_buffer_unsent_bytes_offset_{0};
    bool sending_write_ready_{true}; // flag, indicates that we are waiting for write event
    std::deque<message_ptr> sending_queue_;
    std::chrono::steady_clock::time_point remote_last_activity_;
    uint16_t failed_remote_connect_try_{0};
};


#endif // __REMOTE_SERVICE_HPP__

#ifndef __UTIL_HPP__
#define __UTIL_HPP__

#include <fcntl.h>



inline bool set_socket_nonblocking(const int32_t fd)
{
    int32_t flags{0};
    flags = fcntl(fd, F_GETFD, 0);
    flags |= O_NONBLOCK;
    if (fcntl(fd, F_SETFL, flags) < 0) {
        return false;
    }

    return true;
}


#endif // __UTIL_HPP__

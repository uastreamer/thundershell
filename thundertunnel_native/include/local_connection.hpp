#ifndef __LOCAL_CONNECTION_HPP__
#define __LOCAL_CONNECTION_HPP__

#include <cstdint>
#include <deque>
#include <string>
#include "common.hpp"



class LocalConnection
{
public:
    using write_status = std::tuple<bool, bool, bool>; // first  - write success
                                                       // second - write not ready
                                                       // third  - write failed, connection lost

    LocalConnection(const uint16_t, const std::string&, const uint16_t);
    ~LocalConnection();

    void add_message(message_ptr&&);
    bool connected() const;
    void set_connected();
    uint16_t connection_id() const;
    void create_socket();
    std::string local_host() const;
    uint16_t local_port() const;
    write_status push_message();
    write_status push_message(int64_t);
    write_status push_messages();
    write_status push_unsent_message();
    void set_write_ready();
    void unset_write_ready();
    int32_t socket() const;
    bool write_ready() const;
private:
    int32_t socket_{0};
    uint16_t connection_id_{0};
    std::string local_host_{"127.0.0.1"};
    uint16_t local_port_{0};
    bool connected_{false};
    bool write_ready_{true};
    int64_t unsent_bytes_{0};
    std::deque<message_ptr> sending_queue_;
};


#endif // __LOCAL_CONNECTION_HPP__

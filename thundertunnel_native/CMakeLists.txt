cmake_minimum_required(VERSION 3.0.2)
project(thundertunnel_native)


add_executable(thundertunnel_native
        ./src/thundertunnel_native.cpp
        ./src/local_connection.cpp
        ./src/remote_service.cpp
)


include_directories(${PROJECT_SOURCE_DIR}/include)


target_link_libraries(thundertunnel_native)

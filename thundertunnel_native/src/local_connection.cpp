#include "local_connection.hpp"
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <iostream>
#include <stdexcept>
#include "util.hpp"



LocalConnection::LocalConnection(const uint16_t connection_id, const std::string& local_host,
                                 const uint16_t local_port):
        connection_id_(connection_id), local_host_(local_host), local_port_(local_port)
{
}

//----------------------------------------------------------------------------------------------------------------------

LocalConnection::~LocalConnection()
{
    close(socket_);
}

//----------------------------------------------------------------------------------------------------------------------

void LocalConnection::add_message(message_ptr&& msg)
{
    sending_queue_.emplace_back(std::move(msg));
}

//----------------------------------------------------------------------------------------------------------------------

bool LocalConnection::connected() const
{
    return connected_;
}

//----------------------------------------------------------------------------------------------------------------------

void LocalConnection::set_connected()
{
    connected_ = true;
}

//----------------------------------------------------------------------------------------------------------------------

uint16_t LocalConnection::connection_id() const
{
    return connection_id_;
}

//----------------------------------------------------------------------------------------------------------------------

void LocalConnection::create_socket()
{
    socket_ = ::socket(AF_INET, SOCK_STREAM, 0);
    if (socket_ == -1) {
        throw std::runtime_error("unable to create socket.");
    }

    if (!set_socket_nonblocking(socket_)) {
        throw std::runtime_error("unable to set socket nonblocking.");
    }

    int32_t enable{1};
    setsockopt(socket_, IPPROTO_TCP, TCP_NODELAY, &enable, sizeof(enable));
}

//----------------------------------------------------------------------------------------------------------------------

std::string LocalConnection::local_host() const
{
    return local_host_;
}

//----------------------------------------------------------------------------------------------------------------------

uint16_t LocalConnection::local_port() const
{
    return local_port_;
}

//----------------------------------------------------------------------------------------------------------------------

LocalConnection::write_status LocalConnection::push_message()
{
    auto& msg = sending_queue_.front();

    return push_message(msg->data_len);
}

//----------------------------------------------------------------------------------------------------------------------

LocalConnection::write_status LocalConnection::push_message(const int64_t bytes_to_push)
{
    bool write_success{true};
    bool write_not_ready{false};
    bool write_failed{false};

    auto& msg = sending_queue_.front();
    auto data_len = msg->data_len;
    auto offset = data_len - bytes_to_push;

    auto bytes = write(socket_, msg->data.data() + offset, bytes_to_push);

    if (bytes == data_len) {
        if (bytes_to_push != data_len) {
            unsent_bytes_ = 0; // resetting unsent_bytes_ if we just sent unsent message
        }
        sending_queue_.pop_front();
    } else if (bytes > 0) {
        unsent_bytes_ = data_len - bytes;
        write_success = false;
        write_not_ready = true;
    } else {
        if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
            write_success = false;
            write_not_ready = true;
        } else {
            write_success = false;
            write_failed = true;
        }
    }

    return std::make_tuple(write_success, write_not_ready, write_failed);
}

//----------------------------------------------------------------------------------------------------------------------

LocalConnection::write_status LocalConnection::push_messages()
{
    bool write_success{true};
    bool write_not_ready{false};
    bool write_failed{false};

    // send unsent message
    if (unsent_bytes_) {
        std::tie(write_success, write_not_ready, write_failed) = push_unsent_message();
    }

    while (write_success && !sending_queue_.empty()) {
        std::tie(write_success, write_not_ready, write_failed) = push_message();
    }

    return std::make_tuple(write_success, write_not_ready, write_failed);
}

//----------------------------------------------------------------------------------------------------------------------

LocalConnection::write_status LocalConnection::push_unsent_message()
{
    return push_message(unsent_bytes_); // saved previous unsent bytes
}

//----------------------------------------------------------------------------------------------------------------------

void LocalConnection::set_write_ready()
{
    write_ready_ = true;
}

//----------------------------------------------------------------------------------------------------------------------

void LocalConnection::unset_write_ready()
{
    write_ready_ = false;
}

//----------------------------------------------------------------------------------------------------------------------

int32_t LocalConnection::socket() const
{
    return socket_;
}

//----------------------------------------------------------------------------------------------------------------------

bool LocalConnection::write_ready() const
{
    return write_ready_;
}

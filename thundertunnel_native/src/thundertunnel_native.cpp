#include <signal.h>
#include <iostream>
#include <thread>
#include "remote_service.hpp"



std::string SHELL_HOST{"127.0.0.1"};
uint16_t SHELL_PORT{22};
uint16_t MAX_REMOTE_CONNECT_TRY{5}; // in seconds

uint16_t REMOTE_PORT{18000};
uint16_t MAIN_LOOP_SLEEP{1}; // in seconds


//----------------------------------------------------------------------------------------------------------------------

int main(int argc, char** argv)
{
    signal(SIGPIPE, SIG_IGN);

    if (argc != 3) {
        std::cout << "Usage: " << argv[0] << " <master host> <node name>\n";
        exit(EXIT_FAILURE);
    }

    while (true) {
        try {
            auto remote_host = std::string(argv[1]);
            auto node_name = std::string(argv[2]);

            auto remote_service = RemoteService(std::move(remote_host), REMOTE_PORT, std::move(node_name));
            remote_service.launch();
        } catch (const std::exception& e) {
            std::cerr << e.what() << "\n";
        }

        // avoid flooding
        std::this_thread::sleep_for(std::chrono::seconds(MAIN_LOOP_SLEEP));
    }

    return 0;
}

#include "remote_service.hpp"
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <unistd.h>
#include <array>
#include <iostream>
#include <stdexcept>
#include <thread>
#include "message.hpp"
#include "util.hpp"



extern uint16_t MAX_REMOTE_CONNECT_TRY;


RemoteService::RemoteService(std::string&& remote_host, const uint16_t remote_port, std::string&& node_name):
        remote_host_(std::move(remote_host)), remote_port_(remote_port), node_name_(std::move(node_name))
{
}

//----------------------------------------------------------------------------------------------------------------------

bool RemoteService::io_scheduler_add_socket(const int32_t socket)
{
    ep_event_.data.fd = socket;
    ep_event_.events = EPOLLIN;

    if (epoll_ctl(ep_socket_, EPOLL_CTL_ADD, socket, &ep_event_) == 0) {
        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------

bool RemoteService::io_scheduler_add_socket_out_event(const int32_t socket)
{
    ep_event_.data.fd = socket;
    ep_event_.events |= EPOLLOUT;

    if (epoll_ctl(ep_socket_, EPOLL_CTL_MOD, socket, &ep_event_) == 0) {
        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------

bool RemoteService::io_scheduler_del_socket_out_event(const int32_t socket)
{
    ep_event_.data.fd = socket;
    ep_event_.events &= ~EPOLLOUT;

    if (epoll_ctl(ep_socket_, EPOLL_CTL_MOD, socket, &ep_event_) == 0) {
        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------

bool RemoteService::io_scheduler_del_socket(const int32_t socket)
{
    if (epoll_ctl(ep_socket_, EPOLL_CTL_DEL, socket, &ep_event_) == 0) {
        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------

int64_t RemoteService::get_timeout()
{
    auto timeout = HEARTBEAT_TIMEOUT;

    auto now = std::chrono::steady_clock::now();
    auto ms_from_last_activity =
            std::chrono::duration_cast<std::chrono::milliseconds>(now - remote_last_activity_).count();

    if (ms_from_last_activity < HEARTBEAT_TIMEOUT) {
        timeout = HEARTBEAT_TIMEOUT - ms_from_last_activity;
    }

    return timeout;
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_local_connection_data(const uint16_t connection_id, const int64_t bytes)
{
    auto bytes_left = bytes;
    auto offset = 0;

    while (bytes_left) {
        auto msg = message_ptr_create(message_type::data);
        if (msg) {
            auto data_len = 0;

            if (bytes_left > MESSAGE_DATA_LEN) {
                data_len = MESSAGE_DATA_LEN;
            } else {
                data_len = bytes_left;
            }

            msg->connection_id = connection_id;
            message_ptr_set_data(msg, local_connections_receiving_buffer_.cbegin() + offset, data_len);
            sending_queue_.emplace_back(std::move(msg));

            bytes_left -= data_len;
            offset += data_len;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_local_connection_ended(const uint16_t connection_id)
{
    auto msg = message_ptr_create(message_type::connection_ended_by_dst);
    if (msg) {
        msg->connection_id = connection_id;
        sending_queue_.emplace_back(std::move(msg));
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_messages(std::deque<message_ptr>&& messages)
{
    for (auto&& msg: messages) {
        switch (msg->type) {
            case message_type::heartbeat_reply:
                break;
            case message_type::connection_created:
                handle_message_connection_created(std::move(msg));
                break;
            case message_type::connection_ended_by_src:
                handle_message_connection_ended_by_src(std::move(msg));
                break;
            case message_type::data:
                handle_message_data(std::move(msg));
                break;
            default:
                std::cerr << "[fatal] " << "unknown message, type: " << static_cast<int16_t>(msg->type) << "\n";
                exit(EXIT_FAILURE); // TODO
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_message_connection_created(message_ptr&& msg)
{
    auto connection_id = msg->connection_id;
    make_local_connection(connection_id);
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_message_connection_ended_by_src(message_ptr&& msg)
{
    auto connection_id = msg->connection_id;

    try {
        auto& local_connection = local_connections_by_connection_id_.at(connection_id);
        remove_local_connection(local_connection);
    } catch (...) {
        // TODO: log error
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_message_data(message_ptr&& msg)
{
    auto connection_id = msg->connection_id;

    try {
        auto& local_connection = local_connections_by_connection_id_.at(connection_id);
        local_connection->add_message(std::move(msg));
        if (local_connection->write_ready()) {
            bool write_success{false};
            bool write_not_ready{false};
            bool write_failed{false};

            std::tie(write_success, write_not_ready, write_failed) = local_connection->push_messages();

            if (!write_success) {
                if (write_not_ready) {
                    local_connection->unset_write_ready();
                    io_scheduler_add_socket_out_event(local_connection->socket());
                } else if (write_failed) {
                    remove_local_connection(local_connection);
                }
            }
        }
    } catch (...) {
        // TODO: log error
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_network_event(const int32_t socket, const int32_t ev)
{
    if (ev & EPOLLOUT) {
        handle_socket_write_ready(socket);
    }
    if (ev & EPOLLIN) {
        handle_socket_read_ready(socket);
    }
}

//----------------------------------------------------------------------------------------------------------------------

bool RemoteService::connection_established(const int32_t socket)
{
    bool established{false};

    int32_t err{0};
    socklen_t err_len = sizeof(err);
    if (getsockopt(socket, SOL_SOCKET, SO_ERROR, &err, &err_len) == 0) {
        if (err == 0) {
            established = true;
        }
    }

    return established;
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_socket_write_ready(const int32_t socket)
{
    if (socket == remote_socket_) {
        if (!connected_) {
            if (connection_established(socket)) {
                connected_ = true;
                io_scheduler_del_socket_out_event(socket);
                update_remote_last_activity();
                on_remote_connect();
                failed_remote_connect_try_ = 0;
            } else {
                if (failed_remote_connect_try_ < MAX_REMOTE_CONNECT_TRY) {
                    ++failed_remote_connect_try_;
                }
            }
        } else {
            sending_write_ready_ = true;
            send_to_remote();
        }
    } else {
        try {
            auto& local_connection = local_connections_by_socket_.at(socket);
            if (local_connection->connected()) {
                std::cout << "LOCAL CONNECTION IS NOW ABLE TO RECEIVE DATA\n";

                local_connection->set_write_ready();
                bool write_success{false};
                bool write_not_ready{false};
                bool write_failed{false};

                std::tie(write_success, write_not_ready, write_failed) = local_connection->push_messages();

                if (!write_success) {
                    if (write_not_ready) {
                        local_connection->unset_write_ready();
                        io_scheduler_add_socket_out_event(local_connection->socket());
                    } else if (write_failed) {
                        remove_local_connection(local_connection);
                    }
                }
            } else {
                if (connection_established(socket)) {
                    local_connection->set_connected();
                    io_scheduler_del_socket_out_event(socket);
                    std::cout << "LOCAL CONNECTION ESTABLISHED\n";
                } else {
                    handle_local_connection_ended(local_connection->connection_id());
                }
            }
        } catch (const std::exception& e) {
            std::cerr << "[exception] " << e.what() << "\n";
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_socket_read_ready(const int32_t socket)
{
    if (socket == remote_socket_) {
        read_remote_data();
        update_remote_last_activity();
    } else {
        try {
            auto& local_connection = local_connections_by_socket_.at(socket);

            auto bytes = read(socket, local_connections_receiving_buffer_.data(),
                              local_connections_receiving_buffer_.size());

            if (bytes > 0) {
                handle_local_connection_data(local_connection->connection_id(), bytes);
            } else {
                remove_local_connection(local_connection);
            }
        } catch (const std::exception& e) {
            std::cerr << "[exception] " << e.what() << "\n";
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::handle_timeout_reached()
{
    auto now = std::chrono::steady_clock::now();
    auto ms_from_last_activity =
            std::chrono::duration_cast<std::chrono::milliseconds>(now - remote_last_activity_).count();

    if (ms_from_last_activity >= HEARTBEAT_MAX_TIMEOUT) {
        connected_ = false; // connection lost
    } else {
        send_heartbeat_message();
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::initialize_epoll()
{
    ep_socket_ = epoll_create(ep_event_size_);
    if (ep_socket_ == -1) {
        throw std::runtime_error("Unable to create epoll socket.");
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::launch()
{
    initialize_epoll();
    service_loop();
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::make_local_connection(const uint16_t connection_id)
{
    try {
        // may throw bad_alloc
        auto local_connection = std::make_shared<LocalConnection>(connection_id, "127.0.0.1", 22); // fixme
        // may throw runtime_error
        local_connection->create_socket();
        auto local_socket = local_connection->socket();

        sockaddr_in local_address;
        socklen_t socklen = sizeof(struct sockaddr_in);

        local_address.sin_family = AF_INET;
        local_address.sin_port = htons(local_connection->local_port());
        inet_aton(local_connection->local_host().c_str(), &local_address.sin_addr);

        if ((connect(local_socket, reinterpret_cast<struct sockaddr*>(&local_address), socklen) == -1)
            && (errno == EINPROGRESS))
        {
            io_scheduler_add_socket(local_socket);
            io_scheduler_add_socket_out_event(local_socket);

            local_connections_by_connection_id_.emplace(connection_id, local_connection);
            local_connections_by_socket_.emplace(local_socket, std::move(local_connection));
        } else {
            throw std::runtime_error("unable to make local connection.");
        }
    } catch (const std::exception& e) {
        std::cerr << "[exception] " << e.what() << "\n";
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::make_remote_connection()
{
    if (remote_socket_ != 0) {
        io_scheduler_del_socket(remote_socket_);
        close(remote_socket_);
        remote_socket_ = 0;
        on_remote_disconnect();
    }

    // sleep to avoid flooding (if needed)
    if (failed_remote_connect_try_) {
        std::cout << "[inf] sleep before try to connect: " << failed_remote_connect_try_ << " seconds\n";
        std::this_thread::sleep_for(std::chrono::seconds(failed_remote_connect_try_));
    }

    remote_socket_ = socket(AF_INET, SOCK_STREAM, 0);
    if (remote_socket_ == -1) {
        throw std::runtime_error("unable to create socket.");
    }

    if (!set_socket_nonblocking(remote_socket_)) {
        throw std::runtime_error("unable to set socket nonblocking.");
    }

    int32_t enable{1};
    setsockopt(remote_socket_, IPPROTO_TCP, TCP_NODELAY, &enable, sizeof(enable));

    sockaddr_in remote_address;
    socklen_t socklen = sizeof(struct sockaddr_in);

    remote_address.sin_family = AF_INET;
    remote_address.sin_port = htons(remote_port_);
    inet_aton(remote_host_.c_str(), &remote_address.sin_addr);

    if ((connect(remote_socket_, reinterpret_cast<struct sockaddr*>(&remote_address), socklen) == -1)
        && (errno == EINPROGRESS))
    {
        io_scheduler_add_socket(remote_socket_);
        io_scheduler_add_socket_out_event(remote_socket_);
    } else {
        throw std::runtime_error("unable to make remote connection.");
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::on_remote_connect()
{
    send_hello_message();
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::on_remote_disconnect()
{
    local_connections_by_connection_id_.clear();
    local_connections_by_socket_.clear();
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::process_remote_data(const int64_t bytes)
{
    std::deque<message_ptr> messages;

    auto available_bytes = bytes + receiving_buffer_received_bytes_;

    while (available_bytes) {
        if (available_bytes < MESSAGE_MIN_LEN) {
            break;
        }

        auto msg_ptr = reinterpret_cast<message*>(receiving_buffer_.data() + receiving_buffer_received_bytes_offset_);

        if (message_valid(*msg_ptr)) {
            uint16_t message_len = msg_ptr->data_len + MESSAGE_HEADER_LEN;
            if (message_len <= available_bytes) {
                auto msg = message_ptr_create();
                if (msg) {
                    message_build(msg, receiving_buffer_.data() + receiving_buffer_received_bytes_offset_, message_len);
                    messages.emplace_back(std::move(msg));

                    available_bytes -= message_len;
                    receiving_buffer_received_bytes_offset_ += message_len;
                }
            } else {
                break;
            }
        } else {
            std::cerr << "SYNC NEEDED :(\n";
            exit(EXIT_FAILURE); // TODO: handle error
        }
    }

    if (available_bytes) {
        auto from = receiving_buffer_.begin() + receiving_buffer_received_bytes_offset_;
        auto to = from + available_bytes;
        std::copy(from, to, receiving_buffer_.begin());

        receiving_buffer_received_bytes_ = available_bytes;
    } else {
        receiving_buffer_received_bytes_ = 0;
    }
    receiving_buffer_received_bytes_offset_ = 0;

    handle_messages(std::move(messages));
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::read_remote_data()
{
    int64_t bytes{0};

    if (receiving_buffer_received_bytes_) {
        auto left_bytes = receiving_buffer_.size() - receiving_buffer_received_bytes_;
        bytes = read(remote_socket_, receiving_buffer_.data() + receiving_buffer_received_bytes_, left_bytes);
    } else {
        bytes = read(remote_socket_, receiving_buffer_.data(), receiving_buffer_.size());
    }

    if (bytes > 0) {
        process_remote_data(bytes);
    } else {
        connected_ = false;
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::remove_local_connection(const std::shared_ptr<LocalConnection>& local_connection)
{
    auto socket = local_connection->socket();
    auto connection_id = local_connection->connection_id();

    local_connections_by_socket_.erase(socket);
    local_connections_by_connection_id_.erase(connection_id);

    io_scheduler_del_socket(socket);
    close(socket);
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::send_heartbeat_message()
{
    // sending only if connected and nothing to send else
    if (connected_ && sending_queue_.empty()) {
        auto msg = message_ptr_create(message_type::heartbeat_request);
        if (msg) {
            sending_queue_.emplace_back(std::move(msg));
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::send_hello_message()
{
    auto msg = message_ptr_create(message_type::hello);
    if (msg) {
        message_ptr_set_data(msg, node_name_, node_name_.size());
        sending_queue_.emplace_back(std::move(msg));
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::send_to_remote()
{
    // ensure that we are connected
    if (connected_ && sending_write_ready_) {
        // check if we have unsent data
        if (sending_buffer_unsent_bytes_) {
            send_to_remote_buffered();
        }

        while (sending_write_ready_ && !sending_queue_.empty()) {
            auto& msg = sending_queue_.front();
            auto msg_len = message_ptr_len(msg);

            auto bytes = write(remote_socket_, msg.get(), msg_len);

            if (bytes > 0) {
                if (bytes < msg_len) {
                    auto unsent_bytes = msg_len - bytes;
                    auto offset = bytes;

                    std::memcpy(sending_buffer_.data(), msg.get() + offset, unsent_bytes);
                    sending_buffer_unsent_bytes_ = unsent_bytes;
                    sending_buffer_unsent_bytes_offset_ = offset;
                    sending_write_ready_ = false;
                } else {
                    sending_queue_.pop_front();
                }
            } else {
                sending_write_ready_ = false;
            }
        }

        if (!sending_write_ready_) {
            io_scheduler_add_socket_out_event(remote_socket_);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::send_to_remote_buffered()
{
    auto bytes = write(remote_socket_, sending_buffer_.data() + sending_buffer_unsent_bytes_offset_,
                       sending_buffer_unsent_bytes_);

    if (bytes > 0) {
        if (bytes == sending_buffer_unsent_bytes_) {
            sending_buffer_unsent_bytes_ = 0;
            sending_buffer_unsent_bytes_offset_ = 0;
        } else {
            sending_buffer_unsent_bytes_ -= bytes;
            sending_buffer_unsent_bytes_offset_ += bytes;
            sending_write_ready_ = false;
        }
    } else {
        sending_write_ready_ = false;
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::service_loop()
{
    std::array<struct epoll_event, ep_event_size_> events;

    while (true) {
        try {
            if (!connected_) {
                make_remote_connection();
            }

            send_to_remote();

            auto timeout = get_timeout();

            auto ep_event_count = epoll_wait(ep_socket_, events.data(), ep_event_size_, timeout);
            if (ep_event_count) {
                for (auto i = 0; i < ep_event_count; ++i) {
                    auto ev = events[i].events;
                    auto sock = events[i].data.fd;
                    handle_network_event(sock, ev);
                }
            } else {
                // timeout reached
                handle_timeout_reached();
            }
        } catch (const std::exception& e) {
            std::cerr << "[err] " << e.what() << "\n";
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void RemoteService::update_remote_last_activity()
{
    remote_last_activity_ = std::chrono::steady_clock::now();
}
